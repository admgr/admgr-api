package com.grmnlxndr.admgr.configuration;

import com.grmnlxndr.admgr.domain.Advertising;
import com.grmnlxndr.admgr.repository.AdvertisingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    public CommandLineRunner initDatabase(AdvertisingRepository repository) {
        return args -> {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, 2);

            log.info("Preloading " + repository.save(new Advertising(
                    "Vuelos Baratos",
                    "Vuele por todo el pais deste USD 50",
                    "https://www.telegraph.co.uk/content/dam/Travel/2018/January/white-plane-sky.jpg?imwidth=450",
                    BigDecimal.valueOf(2),
                    BigDecimal.valueOf(25),
                    LocalDate.now().plusDays(2))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Alquileres de autos de lujo",
                    "Disfrute pasear como un rey en las calles",
                    "https://accelerator-origin.kkomando.com/wp-content/uploads/2016/08/rent-car.jpg",
                    BigDecimal.valueOf(2),
                    BigDecimal.valueOf(20),
                    LocalDate.now().plusDays(2))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Horno electricos al mejor precio",
                    "Todas las marcas del mercado en un solo lugar",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpKY3047TG7MEGEmrl1IWTvKna5admpCGTx5ybeKtmqQYadlQK",
                    BigDecimal.valueOf(3),
                    BigDecimal.valueOf(26),
                    LocalDate.now().plusDays(2))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Notebook Gaming",
                    "La potencia en tus manos, ultima generacion y super rapida",
                    "https://images.avell.com.br/media/catalog/product/cache/1/thumbnail/800x600/9df78eab33525d08d6e5fb8d27136e95/a/v/avell-1_13.jpg",
                    BigDecimal.valueOf(2),
                    BigDecimal.valueOf(31),
                    LocalDate.now().plusDays(2))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Ropa deportiva. Primeras marcas",
                    "Consegui todas las marcas y modelos en nuestra tienda online",
                    "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA03/201808/31/00199440711010____3__640x640.jpg",
                    BigDecimal.valueOf(1),
                    BigDecimal.valueOf(17),
                    LocalDate.now().plusDays(2))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Hoteles 5 estrellas",
                    "Los mejores hoteles a los precios mas bajos",
                    "https://s-ec.bstatic.com/images/hotel/max1024x768/681/68184730.jpg",
                    BigDecimal.valueOf(1),
                    BigDecimal.valueOf(15),
                    LocalDate.now().plusDays(3))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Toyota Hilux",
                    "El vehiculo mas vendido en la Argentina. Que estas esperando",
                    "http://www.infoauto.com.ar/files/system/cms/files/files/62200/original/Toyota_Hilux_2019-portada.jpg",
                    BigDecimal.valueOf(4),
                    BigDecimal.valueOf(26),
                    LocalDate.now().plusDays(3))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Vamos Manaos",
                    "La gaseosa mas querida del pais. Vamos Manaos",
                    "https://resizer.iprofesional.com/unsafe/640x360/https://assets.iprofesional.com/assets/jpg/2018/02/453896.jpg?3.0.1.3",
                    BigDecimal.valueOf(3),
                    BigDecimal.valueOf(23),
                    LocalDate.now().plusDays(3))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Smart TV muy grande",
                    "Todos los tamanos, toda la tecnologia",
                    "http://www.nikzer.com/wp-content/uploads/2019/01/925934177s.jpeg",
                    BigDecimal.valueOf(4),
                    BigDecimal.valueOf(35),
                    LocalDate.now().plusDays(3))));

            log.info("Preloading " + repository.save(new Advertising(
                    "Casas prefabricadas de madera",
                    "Hechas a medida, construimos en todo el pais",
                    "https://mlstaticquic-a.akamaihd.net/casas-prefabricadas-de-madera-estilo-americano-D_NQ_NP_653150-MLU29725918252_032019-F.jpg",
                    BigDecimal.valueOf(3),
                    BigDecimal.valueOf(54),
                    LocalDate.now().plusDays(3))));
        };
    }
}
