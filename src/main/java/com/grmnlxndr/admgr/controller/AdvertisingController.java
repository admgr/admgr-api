package com.grmnlxndr.admgr.controller;

import com.grmnlxndr.admgr.domain.Advertising;
import com.grmnlxndr.admgr.service.AdvertisingService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/v1/ads")
public class AdvertisingController {

    private final AdvertisingService advertisingService;

    public AdvertisingController(AdvertisingService advertisingService) {
        this.advertisingService = advertisingService;
    }

    @GetMapping
    public List<Advertising> getAllAds() {
        return advertisingService.getAllAds();
    }

    @PostMapping
    public Advertising addAd(@Valid @RequestBody Advertising ad) {
        return advertisingService.addAd(ad);
    }

    @GetMapping("/random")
    public Advertising getRandomAd(@RequestParam(value = "title", required = false) String title) {
        return advertisingService.getRandomAd(title);
    }

    @GetMapping("/{id}")
    public Advertising getAdById(@PathVariable Long id) {
        return advertisingService.findAdById(id);
    }

    @PutMapping("/{id}")
    public Advertising updateAd(@PathVariable Long id, @RequestBody Advertising newAd) {
        return advertisingService.updateAd(id, newAd);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAd(@PathVariable Long id) {
        advertisingService.deleteAd(id);
    }

}
