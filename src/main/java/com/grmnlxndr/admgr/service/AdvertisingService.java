package com.grmnlxndr.admgr.service;

import com.grmnlxndr.admgr.domain.Advertising;
import com.grmnlxndr.admgr.exception.AdvertisingNotFoundException;
import com.grmnlxndr.admgr.repository.AdvertisingRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdvertisingService {

    @Value("${admgr.weightedRandom.limit:5}")
    @Setter
    private int weightedRandomLimit;

    private final AdvertisingRepository advertisingRepository;

    public AdvertisingService(AdvertisingRepository advertisingRepository) {
        this.advertisingRepository = advertisingRepository;
    }

    public List<Advertising> getAllAds() {
        return advertisingRepository.findAll();
    }

    public Advertising findAdById(Long id) {
        return advertisingRepository.findById(id)
                .orElseThrow(() -> new AdvertisingNotFoundException(id));
    }

    public Advertising addAd(Advertising ad) {
        return advertisingRepository.save(ad);
    }

    public void deleteAd(Long id) {
        if (advertisingRepository.existsById(id)) {
            advertisingRepository.deleteById(id);
        }
    }

    public Advertising updateAd(Long id, Advertising newAd) {
        return advertisingRepository.findById(id)
                .map(ad -> {
                    ad.setTitle(newAd.getTitle());
                    ad.setDescription(newAd.getDescription());
                    ad.setImageUrl(newAd.getImageUrl());
                    ad.setPrice(newAd.getPrice());
                    ad.setCredit(newAd.getCredit());
                    ad.setDueDate(newAd.getDueDate());
                    return advertisingRepository.save(ad);
                })
                .orElseGet(() -> {
                    newAd.setId(id);
                    return advertisingRepository.save(newAd);
                });
    }

    public Advertising getRandomAd(String title) {

        PageRequest pageRequest = PageRequest.of(0, weightedRandomLimit);

        List<Advertising> interestingAds = (title == null) ?
                advertisingRepository.getWeightedRandomAds(pageRequest) :
                advertisingRepository.getWeightedRandomAdsByTitle(title, pageRequest);

        if (interestingAds.isEmpty()) {
            throw new AdvertisingNotFoundException();
        }

        Advertising ad = interestingAds.get((int) (Math.random() * interestingAds.size()));
        ad.setCredit(ad.getCredit().subtract(ad.getPrice()));
        return advertisingRepository.save(ad);
    }
}
