package com.grmnlxndr.admgr.exception;

import lombok.Getter;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
public class ErrorDetails {

    private Date timestamp;
    private String message;
    private List<String> details;

    public ErrorDetails(Date timestamp, String message, String details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = Collections.singletonList(details);
    }

    public ErrorDetails(Date timestamp, String message, List<String> details) {
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
    }
}
