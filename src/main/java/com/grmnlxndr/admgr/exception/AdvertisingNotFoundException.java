package com.grmnlxndr.admgr.exception;

public class AdvertisingNotFoundException extends RuntimeException {

    public AdvertisingNotFoundException() {
        super("Couldn't find any advertising");
    }

    public AdvertisingNotFoundException(Long id) {
        super("Couldn't find advertising with id: " + id);
    }
}
