package com.grmnlxndr.admgr.repository;

import com.grmnlxndr.admgr.domain.Advertising;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdvertisingRepository extends JpaRepository<Advertising, Long> {

    @Query("SELECT a FROM Advertising a WHERE a.price <= a.credit AND a.dueDate >= CURRENT_DATE ORDER BY a.price DESC")
    List<Advertising> getWeightedRandomAds(Pageable pageable);

    @Query("SELECT a FROM Advertising a WHERE lower(a.title) LIKE lower(concat('%', ?1, '%')) AND a.price <= a.credit AND a.dueDate >= CURRENT_DATE ORDER BY a.price DESC")
    List<Advertising> getWeightedRandomAdsByTitle(String filter, Pageable pageable);
}
