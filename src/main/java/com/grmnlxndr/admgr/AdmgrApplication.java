package com.grmnlxndr.admgr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdmgrApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdmgrApplication.class, args);
    }

}
