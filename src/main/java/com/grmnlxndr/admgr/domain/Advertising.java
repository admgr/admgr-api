package com.grmnlxndr.admgr.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Advertising {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "A title must be provided")
    private String title;

    @NotEmpty(message = "A description must be provided")
    private String description;

    private String imageUrl;

    @NotNull(message = "A price must be provided")
    private BigDecimal price;

    @NotNull(message = "A credit must be provided")
    private BigDecimal credit;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull(message = "A due date must be provided")
    private LocalDate dueDate;

    public Advertising(String title,
                       String description,
                       String imageUrl,
                       BigDecimal price,
                       BigDecimal credit,
                       LocalDate dueDate) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.price = price;
        this.credit = credit;
        this.dueDate = dueDate;
    }
}
