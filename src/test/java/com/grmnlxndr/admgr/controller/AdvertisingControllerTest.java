package com.grmnlxndr.admgr.controller;

import com.grmnlxndr.admgr.domain.Advertising;
import com.grmnlxndr.admgr.service.AdvertisingService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AdvertisingControllerTest {

    private Advertising ad;
    private List<Advertising> advertisingList;

    @Mock
    private AdvertisingService advertisingService;

    @InjectMocks
    private AdvertisingController controller;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        ad = Advertising.builder()
                .id(1L)
                .title("title")
                .description("description")
                .imageUrl("imageUrl")
                .price(BigDecimal.ONE)
                .credit(BigDecimal.TEN)
                .dueDate(Date.from(Instant.now()))
                .build();
        advertisingList = Collections.singletonList(ad);
    }

    @Test
    public void shouldGetAllAds() {
        when(advertisingService.getAllAds()).thenReturn(advertisingList);

        List<Advertising> result = controller.getAllAds();
        verify(advertisingService).getAllAds();
        assertEquals(advertisingList, result);
    }

    @Test
    public void shouldAddAd() {
        when(advertisingService.addAd(ad)).thenReturn(ad);

        Advertising result = controller.addAd(ad);
        verify(advertisingService).addAd(ad);
        assertEquals(ad, result);
    }

    @Test
    public void shouldGetRandomAd() {
        when(advertisingService.getRandomAd(anyString())).thenReturn(ad);

        Advertising result = controller.getRandomAd("title");
        verify(advertisingService).getRandomAd(anyString());
        assertEquals(ad, result);
    }

    @Test
    public void shouldGetAdById() {
        when(advertisingService.findAdById(1L)).thenReturn(ad);

        Advertising result = controller.getAdById(1L);
        verify(advertisingService).findAdById(1L);
        assertEquals(ad, result);
    }

    @Test
    public void shouldUpdateAd() {
        when(advertisingService.updateAd(1L, ad)).thenReturn(ad);

        Advertising result = controller.updateAd(1L, ad);
        verify(advertisingService).updateAd(1L, ad);
        assertEquals(ad, result);
    }

    @Test
    public void shouldDeleteAd() {
        controller.deleteAd(1L);

        verify(advertisingService).deleteAd(1L);
    }
}