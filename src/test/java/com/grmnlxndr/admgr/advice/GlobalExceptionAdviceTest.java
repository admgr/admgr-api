package com.grmnlxndr.admgr.advice;

import com.grmnlxndr.admgr.exception.AdvertisingNotFoundException;
import com.grmnlxndr.admgr.exception.ErrorDetails;
import com.grmnlxndr.admgr.exception.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import static org.junit.Assert.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class GlobalExceptionAdviceTest {

    @Mock
    private WebRequest request;

    @Mock
    private HttpHeaders headers;

    @Mock
    private MethodParameter methodParameter;

    @Mock
    private BindingResult bindingResult;

    @InjectMocks
    private GlobalExceptionAdvice advice;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldHandleAdvertisingNotFound() {
        ResponseEntity<?> result = advice.handleAdvertisingNotFound(new AdvertisingNotFoundException(), request);

        ErrorDetails errorDetails = (ErrorDetails) result.getBody();

        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        assertEquals("Couldn't find any advertising", errorDetails.getMessage());
    }

    @Test
    public void shouldHandleResourceNotFound() {
        ResponseEntity<?> result = advice.handleResourceNotFound(new ResourceNotFoundException("Mock message"), request);

        ErrorDetails errorDetails = (ErrorDetails) result.getBody();

        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
        assertEquals("Mock message", errorDetails.getMessage());
    }

    @Test
    public void shouldHandleGlobalException() {
        ResponseEntity<?> result = advice.handleGlobalException(new Exception("Mock message"), request);

        ErrorDetails errorDetails = (ErrorDetails) result.getBody();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
        assertEquals("Mock message", errorDetails.getMessage());
    }

    @Test
    public void handleMethodArgumentNotValid() {
        ResponseEntity<Object> result = advice.handleMethodArgumentNotValid(new MethodArgumentNotValidException(methodParameter, bindingResult),
                headers,
                HttpStatus.BAD_REQUEST,
                request);

        ErrorDetails errorDetails = (ErrorDetails) result.getBody();

        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        assertEquals("Validation Failed", errorDetails.getMessage());
    }
}