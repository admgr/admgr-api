package com.grmnlxndr.admgr.service;

import com.grmnlxndr.admgr.domain.Advertising;
import com.grmnlxndr.admgr.exception.AdvertisingNotFoundException;
import com.grmnlxndr.admgr.repository.AdvertisingRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class AdvertisingServiceTest {

    private Advertising advertising1 = Advertising.builder()
            .id(1L)
            .title("Title 1")
            .description("Description 1")
            .price(BigDecimal.valueOf(5))
            .credit(BigDecimal.valueOf(10))
            .dueDate(new Date())
            .build();

    private Advertising advertising2 = Advertising.builder()
            .id(2L)
            .title("Title 2")
            .description("Description 2")
            .price(BigDecimal.valueOf(3))
            .credit(BigDecimal.valueOf(8))
            .dueDate(new Date())
            .build();

    private List<Advertising> allAdvertising;

    @Mock
    private AdvertisingRepository advertisingRepository;

    @InjectMocks
    private AdvertisingService service;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        allAdvertising = new ArrayList<>(2);
        allAdvertising.add(advertising1);
        allAdvertising.add(advertising2);

        service.setWeightedRandomLimit(5);

        when(advertisingRepository.findAll()).thenReturn(allAdvertising);
        when(advertisingRepository.findById(1L)).thenReturn(Optional.of(advertising1));
        when(advertisingRepository.findById(3L)).thenReturn(Optional.empty());
        when(advertisingRepository.save(advertising1)).thenReturn(advertising1);
        when(advertisingRepository.existsById(1L)).thenReturn(true);
        when(advertisingRepository.existsById(3L)).thenReturn(false);
        when(advertisingRepository.getWeightedRandomAds(any(PageRequest.class))).thenReturn(allAdvertising);
        when(advertisingRepository.getWeightedRandomAdsByTitle(anyString(), any(PageRequest.class))).thenReturn(allAdvertising);
        when(advertisingRepository.getWeightedRandomAdsByTitle(eq("empty"), any(PageRequest.class))).thenReturn(new ArrayList<>());
    }

    @Test
    public void shouldGetAllAds() {
        assertEquals(allAdvertising, service.getAllAds());
        verify(advertisingRepository).findAll();
    }

    @Test
    public void shouldFindAdById() {
        assertEquals(advertising1, service.findAdById(1L));
        verify(advertisingRepository).findById(1L);
    }

    @Test
    public void shouldThrowExceptionWhenTryingToFindANonExistentAd() {
        expectedException.expect(AdvertisingNotFoundException.class);
        service.findAdById(3L);

        expectedException.expectMessage("Couldn't find advertising with id: 3");
    }

    @Test
    public void shouldAddAd() {
        assertEquals(advertising1, service.addAd(advertising1));
        verify(advertisingRepository).save(advertising1);
    }

    @Test
    public void shouldDeleteAd() {
        service.deleteAd(1L);

        verify(advertisingRepository).existsById(1L);
        verify(advertisingRepository).deleteById(1L);
    }

    @Test
    public void shouldNotDeleteAdWhenIdNotExists() {
        service.deleteAd(3L);

        verify(advertisingRepository).existsById(3L);
        verify(advertisingRepository, never()).deleteById(3L);
    }

    @Test
    public void shouldUpdateAd() {
        ArgumentCaptor<Advertising> adCaptor = ArgumentCaptor.forClass(Advertising.class);

        service.updateAd(1L, advertising2);

        verify(advertisingRepository).findById(1L);
        verify(advertisingRepository).save(adCaptor.capture());

        Advertising capturedAd = adCaptor.getValue();

        assertEquals(Long.valueOf(1L), capturedAd.getId());
        assertEquals(advertising2.getTitle(), capturedAd.getTitle());
        assertEquals(advertising2.getDescription(), capturedAd.getDescription());
        assertEquals(advertising2.getImageUrl(), capturedAd.getImageUrl());
        assertEquals(advertising2.getPrice(), capturedAd.getPrice());
        assertEquals(advertising2.getCredit(), capturedAd.getCredit());
        assertEquals(advertising2.getDueDate(), capturedAd.getDueDate());
    }

    @Test
    public void shouldUpdateAdThatDoesNotExist() {
        ArgumentCaptor<Advertising> adCaptor = ArgumentCaptor.forClass(Advertising.class);

        service.updateAd(3L, advertising2);

        verify(advertisingRepository).findById(3L);
        verify(advertisingRepository).save(adCaptor.capture());

        Advertising capturedAd = adCaptor.getValue();

        assertEquals(Long.valueOf(3L), capturedAd.getId());
        assertEquals(advertising2.getTitle(), capturedAd.getTitle());
        assertEquals(advertising2.getDescription(), capturedAd.getDescription());
        assertEquals(advertising2.getImageUrl(), capturedAd.getImageUrl());
        assertEquals(advertising2.getPrice(), capturedAd.getPrice());
        assertEquals(advertising2.getCredit(), capturedAd.getCredit());
        assertEquals(advertising2.getDueDate(), capturedAd.getDueDate());
    }

    @Test
    public void shouldGetRandomAdWithoutTitle() {
        when(advertisingRepository.save(any(Advertising.class))).thenReturn(advertising1);

        Advertising result = service.getRandomAd(null);

        verify(advertisingRepository).getWeightedRandomAds(any(PageRequest.class));
        verify(advertisingRepository).save(any(Advertising.class));
        assertEquals(advertising1, result);
    }

    @Test
    public void shouldGetRandomAdWithTitle() {
        when(advertisingRepository.save(any(Advertising.class))).thenReturn(advertising1);

        Advertising result = service.getRandomAd("title");

        verify(advertisingRepository).getWeightedRandomAdsByTitle(eq("title"), any(PageRequest.class));
        verify(advertisingRepository).save(any(Advertising.class));
        assertEquals(advertising1, result);
    }

    @Test
    public void shouldThrowExceptionWhenNoAdvertisingIsFound() {
        expectedException.expect(AdvertisingNotFoundException.class);

        service.getRandomAd("empty");

        verify(advertisingRepository).getWeightedRandomAdsByTitle(eq("empty"), any(PageRequest.class));
        expectedException.expectMessage("Couldn't find advertising");
    }
}
